# e04-ne1002x-001

IOC settings and startup command to instantiate a New Era syringe pumps NE-1002x IOC; using 'ne1x00' EPICS driver; controlled device is installed in E04 laboratory;